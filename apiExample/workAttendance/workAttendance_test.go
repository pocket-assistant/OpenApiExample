package workAttendance

import (
	. "apiExample/common"
	"fmt"
	"testing"
	"time"
)

func TestGetSignResult(t *testing.T) {

	eTime := time.Now().Unix() * 1000
	sTime := eTime - 3600 * 24 * 30 * 1000 //开始时间一个月前
	r := &getSignRequest{
		Pids: []int64{11}, //11为用户userid
		Start: ToInt32Ptr(0), //必填
		Count: ToInt32Ptr(100), //count
		Stime: ToInt64tr(sTime),
		Etime: ToInt64tr(eTime),
	}

	list, err := GetSignResult(r)
	if err != nil {
		t.Error(err)
	}
	for _, w := range list {
		fmt.Println(w)
	}
}