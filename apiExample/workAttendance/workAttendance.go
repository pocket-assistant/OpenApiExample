package workAttendance

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)

type getSignRequest struct {
	Pids []int64 `json:"pids,omitempty"`
	Gids []int64 `json:"gids,omitempty"`
	Start *int32 `json:"start,omitempty"`
	Count *int32 `json:"count,omitempty"`
	Stime *int64 `json:"stime,omitempty"`
	Etime *int64 `json:"etime,omitempty"`
}

type Position struct {
	Longitude float64 `json:"longitude"`
	Latitude float64 `json:"latitude"`
	Address string `json:"address"`
}

type SignDetail struct {
	Pid int64 `json:"pid"`
	SignWaTime int64 `json:"sign_wa_time"`
	Position *Position `json:"position"`
	RangeType int32 `json:"range_type"`
	TimeType int32 `json:"time_type"`
	AutoSign int32 `json:"auto_sign"`
	WaType int32 `json:"wa_type"`
	Date int32 `json:"date"`
	Index int32 `json:"index"`
	Offset float64 `json:"offset"`
	PointType int32 `json:"point_type"`
	StdSignTime int64 `json:"std_sign_time"`
	BoundType int32 `json:"bound_type"`
}
type NotSignDetail struct {
	Pid int64 `json:"pid"`
	Date int32 `json:"date"`
	Type int32 `json:"type"`
	Index int32 `json:"index"`
}
type Report struct {
	Pid int64 `json:"pid"`
	SignDetails []*SignDetail `json:"sign_details"`
	NotSignDetails []*NotSignDetail `json:"not_sign_details"`
}

type getSignResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Reports []*Report `json:"reports"`
	End int32 `json:"end"`
}


func GetSignResult(r *getSignRequest) ([]*Report, error){
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/workattendance/get_sign_result?access_token=%s", common.ApiUrl, token)

	rsp := &getSignResult{}
	if err := common.HttpPostData(url, r, rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Reports, nil
}