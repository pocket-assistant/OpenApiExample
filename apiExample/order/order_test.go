package order

import (
	"fmt"
	"testing"
)

func TestExportSaleOrder(t *testing.T) {
	orders, err := ExportSaleOrder(0, 30)
	if err != nil {
		t.Error(err)
	}
	for _, o := range orders {
		fmt.Println(o)
	}
}