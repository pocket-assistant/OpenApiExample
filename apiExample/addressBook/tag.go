package addressBook

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)

type createTagRequest struct {
	TagName string `json:"tagname"`
}

type createTagResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	TagId string `json:"tagid"`
}
/*
	4.3.1 创建标签
*/
func CreateTag(name string) (id string, err error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return "", err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/create?access_token=%s", common.ApiUrl, token)

	req := &createTagRequest{
		TagName: name,
	}

	rsp := &createTagResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return "", err
	}

	if rsp.Result != 0 {
		return "", errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return  rsp.TagId, nil
}

type updateTagNameRequest struct {
	TagId string `json:"tagid"`
	TagName string `json:"tagname"`
}

type updateTagNameResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
}
/*
	4.3.2 更新标签
*/
func UpdateTagName(id, name string) error {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/update?access_token=%s", common.ApiUrl, token)

	req := &updateTagNameRequest{
		TagId: id,
		TagName: name,
	}

	rsp := &updateTagNameResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return err
	}

	if rsp.Result != 0 {
		return  errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return nil
}

type deleteTagRequest struct {
	TagId string `json:"tagid"`
}

type deleteTagResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
}

/*
	4.3.3 删除标签
*/
func DeleteTag(id string) error {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/delete?access_token=%s", common.ApiUrl, token)

	req := &deleteTagRequest{
		TagId: id,
	}

	rsp := &deleteTagResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return err
	}

	if rsp.Result != 0 {
		return  errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return nil
}

type addTagUserRequest struct {
	TagId string `json:"tagid"`
	UserId []string `json:"userid"`
	Alias []string `json:"alias"`
}

type addTagUserResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	InvalidUserId []string `json:"invaliduserid"`
	InvalidAlias []string `json:"invalidalias"`
}

/*
	4.3.4 添加成员
*/
func AddTagUser(id string, users []string, alias []string) ([]string, []string, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/add_member?access_token=%s", common.ApiUrl, token)

	req := &addTagUserRequest{
		TagId: id,
		UserId: users,
		Alias: alias,
	}

	rsp := &addTagUserResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return users, alias, err
	}

	if rsp.Result != 0 {
		return  rsp.InvalidUserId, rsp.InvalidAlias, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.InvalidUserId, rsp.InvalidAlias, nil
}

type delTagUserRequest struct {
	TagId string `json:"tagid"`
	UserId []string `json:"userid"`
	Alias []string `json:"alias"`
}

type delTagUserResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	InvalidUserId []string `json:"invaliduserid"`
	InvalidAlias []string `json:"invalidalias"`
}

/*
	4.3.5 删除成员
*/
func DelTagUser(id string, users []string, alias []string) ([]string, []string, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/del_member?access_token=%s", common.ApiUrl, token)

	req := &delTagUserRequest{
		TagId: id,
		UserId: users,
		Alias: alias,
	}

	rsp := &delTagUserResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return users, alias, err
	}

	if rsp.Result != 0 {
		return  rsp.InvalidUserId, rsp.InvalidAlias, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.InvalidUserId, rsp.InvalidAlias, nil
}

type getTagMembersResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	TagName string `json:"tagname"`
	Member []*UserInfo `json:"member"`
}

/*
	4.3.6 获取成员
*/
func GetTagMembers(id string) ([]*UserInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/get?access_token=%s&tagid=%s", common.ApiUrl, token, id)

	rsp := &getTagMembersResult{}
	if err := common.HttpGetData(url, rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}

	return rsp.Member, nil
}

type TagInfo struct {
	TagId string `json:"tagid"`
	TagName string `json:"tagname"`
}

type getTagListResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	TagList []*TagInfo `json:"taglist"`
}

/*
	4.3.7 获取标签列表
*/
func GetTagList() ([]*TagInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/roster/tag/list?access_token=%s", common.ApiUrl, token)

	rsp := &getTagListResult{}
	if err := common.HttpGetData(url, rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}

	return rsp.TagList, nil
}