package addressBook

import (
	"fmt"
	"testing"
)

func TestGetUserInfoByAlias(t *testing.T) {
	/*
		根据别名获取用的时候，需要申请应用的时候标注，用什么做别名
		本测试案列的APPId使用的是账号作为别名，故这里演示采用手机号
		别名获取目前支持手机号和工号
	 */
	account := "73373373301"
	user, err := GetUserInfoByAlias(account)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(user)
		user, err = GetUserInfoById(user.UserId)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(user)
		}
	}
}

func TestGetUserInfoByCode(t *testing.T) {
	/*
		Code来源从app跳转到第三方应用的时候携带的code=xxx字段，测试的时候需要动态获取
		5分钟内，一次有效
	 */
	code := "12312"
	user, err := GetUserInfoByCode(code)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(user)
	}
}