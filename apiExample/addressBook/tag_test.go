package addressBook

import (
	"fmt"
	"testing"
)

func TestTag(t *testing.T) {
	TagName := "testTag"
	TagId, err := CreateTag(TagName)
	if err != nil {
		fmt.Println(err)
	} else {
		defer func(){
			fmt.Println(DeleteTag(TagId))
			fmt.Println(GetTagList())
		}()
		TagName = "testTagModify"
		fmt.Println(UpdateTagName(TagId, TagName))
		fmt.Println(GetTagList())
		alias := []string{
			"73373373301",
			"73373373302",
		}
		fmt.Println(AddTagUser(TagId, nil, alias))
		fmt.Println(GetTagMembers(TagId))
		fmt.Println(DelTagUser(TagId, nil, alias))
		fmt.Println(GetTagMembers(TagId))
	}
}