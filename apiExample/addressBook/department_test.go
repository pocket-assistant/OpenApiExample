package addressBook

import (
	"fmt"
	"testing"
)

func TestGetDepartmentInfo(t *testing.T) {
	/*
	 跟部门的Id是1
	 */
	department, err := GetDepartmentInfo("1")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(department)
	}
}

func TestGetDepartmentMember(t *testing.T) {

	users, err := GetDepartmentMember("1", 0)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(users)
	}
}

func TestGetDepartmentList(t *testing.T) {

	departments, err := GetDepartmentList("0", 1)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(departments)
	}
}