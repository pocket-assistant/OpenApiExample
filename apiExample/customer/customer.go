package customer

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)


type property struct {
	PropId *int64 `json:"prop_id,omitempty"`
	DataType *int32 `json:"data_type,omitempty"`
	PropName string `json:"prop_name,omitempty"`
	PropValue string `json:"prop_value,omitempty"`
	OffOn *int32 `json:"off_on,omitempty"` //是否开启
	PropPosition *int32 `json:"prop_position,omitempty"`
	MustFill *int32 `json:"must_fill,omitempty"`
	SortId *int32 `json:"sort_id,omitempty"`
}

type contact struct {
	Contactid *int64 `json:"contactid,omitempty"`
	Name string `json:"name,omitempty"`
	Sex *int32 `json:"sex,omitempty"`
	Titles []string `json:"titles,omitempty"`
	Mobiles []string `json:"mobiles,omitempty"`
	Phones []string `json:"phones,omitempty"`
	Ims []string `json:"ims,omitempty"`
	Mails []string `json:"mails,omitempty"`
	Faxs []string `json:"faxs,omitempty"`
	Note string `json:"note,omitempty"`
	Address string `json:"address,omitempty"`
	Hobby string `json:"hobby,omitempty"`
	Wechats []string `json:"wechats,omitempty"`
	Weibos []string `json:"weibos,omitempty"`
	Birthday *int64 `json:"birthday,omitempty"`
	Properties []*property `json:"properties,omitempty"`
}

type follower struct {
	Pid *int64 `json:"pid,omitempty"`
	Fctype int32 `json:"fctype,omitempty"` //编辑的时候注意，0为新增，1为删除，默认为0
}

type customLabel struct {
	LblId *int32 `json:"lbl_id,omitempty"`
	LblName string `json:"lbl_name,omitempty"`
	SortId *int32 `json:"sort_id,omitempty"`
}

type labelRecord struct {
	Lbl *customLabel `json:"lbl,omitempty"`
	SubLbls []*labelRecord `json:"sub_lbls,omitempty"`
}

type labelDoc struct {
	Clbls []*labelRecord `json:"clbls,omitempty"`
	Version int64 `json:"version,omitempty"`
}

type customer struct {
	Custmid *int64 `json:"custmid,omitempty"`
	Name string `json:"name,omitempty"`
	Addr string `json:"addr,omitempty"`
	Contacts []*contact `json:"contacts,omitempty"`
	Followers []*follower `json:"followers,omitempty"`
	Custmno string `json:"custmno,omitempty"`
	Websites []string `json:"websites,omitempty"`
	Introduction string `json:"introduction,omitempty"`
	CreatePid *int64 `json:"create_pid,omitempty"`
	CreateTime *int64 `json:"create_time,omitempty"`
	ModifyPid *int64 `json:"modify_pid,omitempty"`
	ModifyTime *int64 `json:"modify_time,omitempty"`
	LblDoc *labelDoc `json:"lbl_doc,omitempty"`
	Properties []*property `json:"properties,omitempty"`
}

type contactCtrl struct {
	Contactid *int64 `json:"contactid"`
	Name bool `json:"name,omitempty"`
	Sex bool `json:"sex,omitempty"`
	Title bool `json:"title,omitempty"`
	Mobile bool `json:"mobile,omitempty"`
	Phone bool `json:"phone,omitempty"`
	Im bool `json:"im,omitempty"`
	Mail bool `json:"mail,omitempty"`
	Fax bool `json:"fax,omitempty"`
	Note bool `json:"note,omitempty"`
	Address bool `json:"address,omitempty"`
	Hobby bool `json:"hobby,omitempty"`
	Wechat bool `json:"wechat,omitempty"`
	Weibo bool `json:"weibo,omitempty"`
	Birthday bool `json:"birthday,omitempty"`
	Properties bool `json:"properties,omitempty"`
}
type customerCtrl struct {
	Name bool `json:"name,omitempty"`
	Addr bool `json:"addr,omitempty"`
	Contacts []*contactCtrl `json:"contacts,omitempty"`
	Followers bool `json:"followers,omitempty"`
	Lbl bool `json:"lbl,omitempty"`
	Custmno bool `json:"custmno,omitempty"`
	Websites bool `json:"websites,omitempty"`
	Introduction bool `json:"introduction,omitempty"`
	Properties bool `json:"properties,omitempty"`
	/*
		其他新增的字段，自行处理
	*/
}

type exportCustomerResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Custms []*customer `json:"custms"`
}
/*
	7.1 导出客户
 */
func ExportCustomer(start, count int32) ([]*customer, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/export?access_token=%s", common.ApiUrl, token)

	rsp := &exportCustomerResult{}
	if err := common.HttpGetData(url, rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}

	return rsp.Custms, nil
}

type importCustomerRequest struct {
	OpPid int64 `json:"op_pid"`
	Custms []*customer `json:"custms"`
}
type ImportCustomerDetailInfo struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Custmid int64 `json:"custmid"`
}
type importCustomerResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Infos []*ImportCustomerDetailInfo `json:"infos"`
}
/*
	7.2 导入客户
*/
func ImportCustomer(req *importCustomerRequest) ([]*ImportCustomerDetailInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/import?access_token=%s", common.ApiUrl, token)


	rsp := &importCustomerResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Infos, nil
}

type getCustomerLableInfoResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	LblDoc *labelDoc `json:"lbl_doc"`
}
/*
	7.3 获取公司客户标签设置
*/
func GetCustomerLableInfo() (*labelDoc, error) {

	token, err := auth.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/getlabel?access_token=%s", common.ApiUrl, token)

	rsp := &getCustomerLableInfoResult{}
	if err := common.HttpGetData(url, rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}

	return rsp.LblDoc, nil
}

type modifyCustomerRequest struct {
	Custm *customer `json:"custm"`
	Custctrol *customerCtrl `json:"custctrol"`
	OpPid int64 `json:"op_pid"`
}
type modifyCustomerResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Custm *customer `json:"custm"`
}
/*
	7.4 修改客户信息
*/
func ModifyCustomer(req *modifyCustomerRequest) (*customer, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/modify?access_token=%s", common.ApiUrl, token)

	rsp := &modifyCustomerResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Custm, nil
}

type queryCustomerIdRequest struct {
	Phones []string `json:"phones"`
}

type queryCustomerInfo struct {
	Phone string `json:"phone"`
	Custmids []int64 `json:"custmids"`
}

type queryCustomerIdResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Infos []*queryCustomerInfo `json:"infos"`
}
/*
	7.5 查询客户id
*/
func QueryCustomerId(phones []string) ([]*queryCustomerInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/query?access_token=%s", common.ApiUrl, token)

	req := &queryCustomerIdRequest{
		Phones: phones,
	}

	rsp := &queryCustomerIdResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Infos, nil
}

type getCustomerRequest struct {
	Custmids []int64 `json:"custmids"`
}

type getCustomerResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Custms []*customer `json:"custms"`
}
/*
  7.6 查询客户信息
*/
func GetCustomerInfo(ids []int64) ([]*customer, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/customer/getcustm?access_token=%s", common.ApiUrl, token)

	req := &getCustomerRequest{
		Custmids: ids,
	}

	rsp := &getCustomerResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Custms, nil
}