package customer

import (
	. "apiExample/common"
	"fmt"
	"testing"
)

func TestExportCustomer(t *testing.T) {
	customers , err := ExportCustomer(0, 10)
	if err != nil{
		t.Fail()
	}
	for _, c := range  customers {
		fmt.Println(c)
	}
}

func TestImportCustomer(t *testing.T) {
	req := []*customer{
		{
			Custmid: ToInt64tr(0),
			Name: "导入客户1",
			Addr: "遥远的东方",
			Followers: []*follower{
				{Pid: ToInt64tr(11)},
			},
			CreatePid: ToInt64tr(11),
			Properties: []*property{
				{
					PropId: ToInt64tr(212),
					PropValue: "123123",
				},
				{
					PropId: ToInt64tr(213),
					PropValue: "ceshi",
				},
			},
			Contacts: []*contact{
				{
					Contactid: ToInt64tr(1),
					Name: "我是联系人",
					Sex: ToInt32Ptr(0),
					Mobiles: []string{"12345678901", "22345678901"},
					Properties: []*property{
						{
							PropId: ToInt64tr(214),
							PropValue: "ceshi2",
						},
					},
				},
			},

		},
	}

	infos, err := ImportCustomer(&importCustomerRequest{11,req})
	if err != nil {
		t.Error(err)
	}
	for _, v := range infos {
		fmt.Println(v)
	}
}

func TestGetCustomerLableInfo(t *testing.T) {
	labl, err := GetCustomerLableInfo()
	if err != nil {
		t.Error(err)
	}
	fmt.Println(labl)
}

func TestModifyCustomer(t *testing.T) {
	req := &customer{
		Custmid: ToInt64tr(7),
		Name: "导入客户2",
		Addr: "遥远的东方,编辑",
		Followers: []*follower{
			{
				Pid: ToInt64tr(12),
				Fctype: 0,
			},
			{
				Pid: ToInt64tr(11),
				Fctype: 1,    //1为删除
			},
		},
		Properties: []*property{
			{
				PropId: ToInt64tr(212),
				PropValue: "99999",
			},
			{
				PropId: ToInt64tr(213),
				PropValue: "ceshi编辑",
			},
		},
		Contacts: []*contact{
			{
				Contactid: ToInt64tr(1),
				Name: "我是联系人编辑",
				Mobiles: []string{"12345678902", "22345678901"},
				Properties: []*property{
					{
						PropId: ToInt64tr(214),
						PropValue: "ceshi2编辑",
					},
				},
			},
		},
		LblDoc: &labelDoc{
			Clbls: []*labelRecord{
				{
					Lbl: &customLabel{
						LblId: ToInt32Ptr(3),
						LblName: "通话状态",
					},
					SubLbls: []*labelRecord{
						{
							Lbl: &customLabel{
								LblId: ToInt32Ptr(113),
								LblName: "已接听",
							},
						},
					},
				},
				{
					Lbl: &customLabel{
						LblId: ToInt32Ptr(1),
						LblName: "客户来源",
					},
					SubLbls: []*labelRecord{
						{
							Lbl: &customLabel{
								LblId: ToInt32Ptr(104),
								LblName: "线下广告",
							},
						},
					},
				},
			},
		},

	}
	reqCtrl := &customerCtrl{
		Name: true,
		Addr: true,
		Followers: true,
		Properties:true,
		Lbl: true,
		Contacts: []*contactCtrl{
			{
				Contactid: ToInt64tr(1),
				Name: true,
				Mobile: true,
				Properties: true,
			},
		},
	}

	c, err := ModifyCustomer(&modifyCustomerRequest{req, reqCtrl, 11})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(c)
}

func TestQueryCustomerId(t *testing.T) {
	customers, err := QueryCustomerId([]string{"12345678901","130956465318"})
	if err != nil{
		t.Fail()
	}
	for _, c := range  customers {
		fmt.Println(c)
	}
}

func TestGetCustomerInfo(t *testing.T) {
	customers, err := GetCustomerInfo([]int64{7})
	if err != nil{
		t.Fail()
	}
	for _, c := range  customers {
		fmt.Println(c)
	}

}