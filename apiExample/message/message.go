package message

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)

type postTextMessageRequest struct {
	ToUser []string `json:"to_user,omitempty"`
	ToAlias []string `json:"to_alias,omitempty"`
	ToDepartment []string `json:"to_department,omitempty"`
	ToTag []string `json:"to_tag,omitempty"`
	Type string `json:"type"`
	Content string `json:"content"`
}

type postTextMessageResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
}

/*
  6.1.1 文本消息
*/
func PostTextMessage(user []string, alias []string, department []string, tag []string, content string) error {

	token, err := auth.GetAccessToken()
	if err != nil {
		return  err
	}

	url := fmt.Sprintf("%s/cgi-bin/im/send?access_token=%s", common.ApiUrl, token)

	req := &postTextMessageRequest{
		ToUser: user,
		ToAlias: alias,
		ToDepartment: department,
		ToTag: tag,
		Type: "text",
		Content: content,
	}

	rsp := &postTextMessageResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return err
	}

	if rsp.Result != 0 {
		return errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return nil
}

type iTextPicture struct {
	MediaId string `json:"media_id"`
	Size int32 `json:"size"`
	Width int32 `json:"width"`
	Height int32 `json:"height"`
}

type iTextInfo struct {
	Title string `json:"title"`
	Content string `json:"content"`
	Url string `json:"url"`
	Picture *iTextPicture `json:"picture"`
}

type postItextMessageRequest struct {
	ToUser []string `json:"to_user"`
	ToAlias []string `json:"to_alias"`
	ToDepartment []string `json:"to_department"`
	ToTag []string `json:"to_tag"`
	Type string `json:"type"`
	Info *iTextInfo `json:"info"`
}

type postItextMessageResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
}

/*
	6.1.2 图文消息
*/
func PostItextMessage(user []string, alias []string, department []string, tag []string, info *iTextInfo) error {

	token, err := auth.GetAccessToken()
	if err != nil {
		return  err
	}

	url := fmt.Sprintf("%s/cgi-bin/im/send?access_token=%s", common.ApiUrl, token)

	req := &postItextMessageRequest{
		ToUser: user,
		ToAlias: alias,
		ToDepartment: department,
		ToTag: tag,
		Type: "itext",
		Info: info,
	}

	rsp := &postItextMessageResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return err
	}

	if rsp.Result != 0 {
		return errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return nil
}

type aMsgInfo struct {
	Content string `json:"content"`
	Tag string `json:"tag"`
	Url string `json:"url"`
}

type postAmsgMessageRequest struct {
	ToUser []string `json:"to_user"`
	ToAlias []string `json:"to_alias"`
	ToDepartment []string `json:"to_department"`
	ToTag []string `json:"to_tag"`
	Type string `json:"type"`
	Info *aMsgInfo `json:"info"`
}

type postAmsgMessageResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
}

/*
	6.1.3 审核消息
*/
func PostAmsgMessage(user []string, alias []string, department []string, tag []string, info *aMsgInfo) error {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  err
	}

	url := fmt.Sprintf("%s/cgi-bin/im/send?access_token=%s", common.ApiUrl, token)

	req := &postAmsgMessageRequest{
		ToUser: user,
		ToAlias: alias,
		ToDepartment: department,
		ToTag: tag,
		Type: "amsg",
		Info: info,
	}

	rsp := &postAmsgMessageResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return err
	}

	if rsp.Result != 0 {
		return errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return nil
}