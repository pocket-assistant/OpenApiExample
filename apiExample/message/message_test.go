package message

import (
	"apiExample/addressBook"
	"apiExample/media"
	"fmt"
	"testing"
)

func TestPostTextMessage(t *testing.T) {
	/*
		根据别名获取用的时候，需要申请应用的时候标注，用什么做别名
		本测试案列的APPId使用的是账号作为别名，故这里演示采用手机号
		别名获取目前支持手机号和工号
	*/
	alias := []string{"12345678901", "12345678902"}

	fmt.Println(PostTextMessage(nil, alias, nil,nil, "this test msg alias "))

	department := []string{"1"}
	fmt.Println(PostTextMessage(nil, nil, department,nil, "this test msg department"))

	user := []string{"11","12"}
	fmt.Println(PostTextMessage(user, nil, nil,nil, "this test msg user"))

	TagName := "testTag"
	TagId, err := addressBook.CreateTag(TagName)
	if err != nil {
		fmt.Println(err)
	} else {
		defer func(){
			fmt.Println(addressBook.DeleteTag(TagId))
		}()
		alias := []string{
			"12345678901",
			"12345678902",
		}
		fmt.Println(addressBook.AddTagUser(TagId, nil, alias))
		fmt.Println(PostTextMessage(user, nil, nil,[]string{TagId}, "this test msg tag"))
	}

}


func TestPostTextMessage2(t *testing.T) {
	/*
		根据别名获取用的时候，需要申请应用的时候标注，用什么做别名
		本测试案列的APPId使用的是账号作为别名，故这里演示采用手机号
		别名获取目前支持手机号和工号
	*/
	user := []string{"6138660"}
	fmt.Println(PostTextMessage(user, nil, nil,nil, "这是一条测试消息，如果收到麻烦告知吕珊珊，谢谢"))
}

func TestPostItextMessage(t *testing.T) {
	fileName := "D:\\图片\\xxx.gif"
	mediaId, err := media.UploadMedia(fileName, "image")
	if err != nil {
		t.Fail()
	}
	info := iTextInfo{
		Title: "this is test title",
		Content: "this is test content",
		Url: "web.kdzl.cn",
		Picture: &iTextPicture{
			MediaId: mediaId, //这里的属性为测试图片的，正常自行设置
			Size: 6666,
			Height: 80,
			Width: 80,
		},
	}

	alias := []string{"12345678901", "12345678902"}

	fmt.Println(PostItextMessage(nil, alias, nil,nil, &info))
}


func TestPostAmsgMessage(t *testing.T) {
	info := aMsgInfo{
		Content: "this is test content",
		Url: "web.kdzl.cn",
		Tag: "this is tag!",
	}

	alias := []string{"12345678901", "12345678902"}

	fmt.Println(PostAmsgMessage(nil, alias, nil,nil, &info))
}