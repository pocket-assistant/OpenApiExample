package workflow

import (
	"apiExample/common"
	"fmt"
	"testing"
)

func TestExportWorkflow(t *testing.T) {

	req := &exportWorkflowRequest{
		Type: common.ToInt32Ptr(1),
		Pid: 11,
		Start: common.ToInt32Ptr(0),
		Limit: 2,
	}

	list, err := ExportWorkflow(req)
	if err != nil {
		t.Error(err)
	}
	for _, w := range list {
		fmt.Println(w)
	}
}