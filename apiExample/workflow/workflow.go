package workflow

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)


type WorkflowFrom struct {
	ItemId int32 `json:"itemId"`
	Label string `json:"label"`
	Xtype string `json:"xtype"`
	Data string `json:"data"`
}

type WorkflowComment struct {
	TaskId string `json:"taskId"`
	TaskName string `json:"taskName"`
	Reason string `json:"reason"`
	Opt int32 `json:"opt"`
	Opinion int32 `json:"opinion"`
	DealTime int64 `json:"dealTime"`
	Pid int64 `json:"pid"`
	UserName string `json:"userName"`
}

type WorkflowInfo struct {
	ProcessInstId string `json:"processInstId"`
	SubmitUser int64 `json:"submitUser"`
	SubmitUserName string `json:"submitUserName"`
	DepartmentName string `json:"departmentName"`
	SubmitTime int64 `json:"submitTime"`
	EndTime int64 `json:"endTime"`
	ProcessDefineId int64 `json:"processDefineId"`
	ProcessNameId string `json:"processNameId"`
	ProcessName string `json:"processName"`
	ProcessStatus int32 `json:"processStatus"`
	WorkflowNum string `json:"workflowNum"`
	From []*WorkflowFrom `json:"from"`
	Comments []*WorkflowComment `json:"comments"`
}

type exportWorkflowRequest struct {
	Type *int32 `json:"type,omitempty"`
	Status string `json:"status,omitempty"`
	StartTime string `json:"startTime,omitempty"`
	EndTime string `json:"endTime,omitempty"`
	Pid int64 `json:"pid,omitempty"`  //用户ID和UserID是一个意思
	Start *int32 `json:"start,omitempty"`
	Limit int32 `json:"limit,omitempty"`

}

type exportWorkflowResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	RetTime int64 `json:"retTime"`
	List []*WorkflowInfo `json:"list"`
}
/*
	8.1 流程导出
*/
func ExportWorkflow(req *exportWorkflowRequest) ([]*WorkflowInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/workflow/exportProcess?access_token=%s", common.ApiUrl, token)

	rsp := &exportWorkflowResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.List, nil
}