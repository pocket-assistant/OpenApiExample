package cusTalk

import (
	"apiExample/auth"
	"apiExample/common"
	"errors"
	"fmt"
)

type exportCustalkRequest struct {
	Pids []int64 `json:"pids,omitempty"`
	Gids []int64 `json:"gids,omitempty"`
	Skip *int32 `json:"skip,omitempty"`
	Count *int32 `json:"count,omitempty"`
	Stime *int64 `json:"stime,omitempty"`
	Etime *int64 `json:"etime,omitempty"`
	Cusids []int64 `json:"cusids,omitempty"`
}

type CusTalk struct {
	Tkid int64 `json:"tkid"`
	Did int64 `json:"did"`
	Uid int64 `json:"uid"`
	Gid int64 `json:"gid"`
	Ver int32 `json:"ver"`
	Cusid int32 `json:"cusid"`
	Content string `json:"content"`
	PublishTime int64 `json:"publish_time"`
	ModifyTime int64 `json:"modify_time"`
}

type exportCustalkResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	End bool `json:"end"`
	Custalks []*CusTalk `json:"custalks"`
}
func ExportCustalk(req *exportCustalkRequest) ([]*CusTalk, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/custalk/export?access_token=%s", common.ApiUrl, token)


	rsp := &exportCustalkResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Custalks, nil
}

type importCustalkRequest struct {
	Custalks []*CusTalk `json:"custalks"`
}

type importCustalkInfo struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	Tkid int64 `json:"tkid"`
}

type importCustalkResult struct {
	Result int32 `json:"result"`
	Errmsg string `json:"errmsg"`
	End bool `json:"end"`
	Infos []*importCustalkInfo `json:"infos"`
}
func ImportCustalk(req *importCustalkRequest) ([]*importCustalkInfo, error) {
	token, err := auth.GetAccessToken()
	if err != nil {
		return  nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/custalk/import?access_token=%s", common.ApiUrl, token)


	rsp := &importCustalkResult{}
	if err := common.HttpPostData(url, req ,rsp); err != nil {
		return nil, err
	}

	if rsp.Result != 0 {
		return  nil, errors.New(fmt.Sprintf("Server respone not ok, result=%v,msg=%s", rsp.Result, rsp.Errmsg))
	}
	return rsp.Infos, nil
}