package cusTalk

import (
	"fmt"
	"testing"
	. "apiExample/common"
)

func TestExportCustalk(t *testing.T) {
	req := &exportCustalkRequest{
		Count: ToInt32Ptr(5),
		Skip: ToInt32Ptr(0),
	}

	cus, err := ExportCustalk(req)
	if err != nil {
		t.Error(err)
	}
	for _, v := range cus {
		fmt.Println(v)
	}
}

func TestImportCustalk(t *testing.T) {
	req := &importCustalkRequest{
		Custalks: []*CusTalk{
			{
				Did: 10000,
				Uid: 11,
				Cusid: 5,
				Content: "1231231231",
			},
		},
	}
	infos, err := ImportCustalk(req)
	if err != nil {
		t.Error(err)
	}
	for _, v := range infos {
		fmt.Println(v)
	}
}