package media

import (
	"fmt"
	"testing"
)

func TestUploadMedia(t *testing.T) {
	fileName := "D:\\图片\\xxx.gif"
	fmt.Println(UploadMedia(fileName, "image"))
	//FjHnq8xhYcN_cfbbly_uuhMppLUf <nil>
}

func TestDownloadMedia(t *testing.T) {
	fileName := "D:\\图片\\xxx.gif"
	mediaId, err := UploadMedia(fileName, "image")
	if err != nil {
		t.Fail()
	}

	outFile := "D:\\图片\\xxx2_test.gif"
	fmt.Println(DownloadMedia(mediaId, outFile))
}

func TestDownloadMediaUrl(t *testing.T) {
	fileName := "D:\\图片\\xxx.gif"
	mediaId, err := UploadMedia(fileName, "image")
	if err != nil {
		t.Fail()
	}
	fmt.Println(DownloadMediaUrl([]string{mediaId}))
}