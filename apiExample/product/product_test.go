package product

import (
	"fmt"
	"testing"
)

func TestExportProduct(t *testing.T) {
	products, err := ExportProduct(0, 30)
	if err != nil {
		t.Error(err)
	}
	for _, v := range products {
		fmt.Println(v)
	}
}

func TestGetProductDetail(t *testing.T) {
	req := &getProductDetailRequest{
		ProductInfos: []*productInfo{
			{
				ProductId: 11,
				Version: 1,
			},
		},
	}
	products, err := GetProductDetail(req)
	if err != nil {
		t.Error(err)
	}
	for _, v := range products {
		fmt.Println(v)
	}
}